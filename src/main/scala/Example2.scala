

object Example2 {

  trait MyOption[+A] {
    def map[B](f: A => B): MyOption[B]
    def flatMap[B](f: A => MyOption[B]): MyOption[B]
    def getOrElse[B >: A](default: => B): B
    def orElse[B >: A](ob: => MyOption[B]): MyOption[B]
    def filter(f: A => Boolean): MyOption[A]
  }

  case class MySome[+A](get: A) extends MyOption[A] {
    def map[B](f: A => B) = MySome(f(get))
    def flatMap[B](f: A => MyOption[B]) = MySome(f(get)).get
    def getOrElse[B >: A](default: => B): B = get
    def orElse[B >: A](ob: => MyOption[B]): MyOption[B] = this
    def filter(f: A => Boolean) = if (f(get)) this else MyNone
  }

  case object MyNone extends MyOption[Nothing] {
    def map[B](f: Nothing => B) = MyNone
    def flatMap[B](f: Nothing => MyOption[B]) = MyNone
    def getOrElse[B >: Nothing](default: => B): B = default
    def orElse[B >: Nothing](ob: => MyOption[B]): MyOption[B] = ob
    def filter(f: Nothing => Boolean) = MyNone
  }

  def sequence[A](a: List[MyOption[A]]): MyOption[List[A]] = {
    val nones = a.collect { case MyNone => true }
    if (nones.nonEmpty) MyNone else MySome(a.collect { case MySome(s) => s })
  }

  def traverse[A, B](a: List[A])(f: A => MyOption[B]): MyOption[List[B]] = {
    sequence(a.map(a1 => f(a1)))
  }

  def tryo[A](a: => A): MyOption[A] =
    try MySome(a)
    catch { case e: Exception => MyNone }

  def main(args: Array[String]): Unit = {
//    val seq1 = sequence(List(MySome(1), MySome(2), MySome(3), MyNone))
//    val seq2 = sequence(List(MySome(1), MySome(2), MySome(3)))
//    print(s"\n\n$seq1\n\n$seq2\n\n")

    val someInts = List("1", "2", "3", "not an int", "4", "5")
    val parsedInts = sequence(someInts map (i => tryo(i.toInt)))
    val someMoreInts = List("1", "2", "3", "4", "5")
    val validInts = sequence(someMoreInts map (i => tryo(i.toInt)))
    print(s"\n\n$parsedInts\n\n$validInts\n\n")
  }
}