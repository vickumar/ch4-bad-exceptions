
object Example1 {
  def failingFn(i: Int): Int = {
    val y: Int = throw new Exception("I have sumarrily failed!")
    try {
      val x = 42 + 5
      x+y
    }
    catch { case e: Exception => 43 }
  }

  def failingFn2(i: Int): Int = {
    try {
      val x = 42 + 5
      x + ((throw new Exception("I have sumarrily failed!")): Int) }
    catch { case e: Exception => 43 }
  }

  def main(args: Array[String]): Unit = {
    //print(s"\n\n${ failingFn(12) }\n\n")
    print(s"\n\n${failingFn2(12) }\n\n")
  }
}