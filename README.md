How to run the examples?
========================

From the project root:

```
brew install scala sbt
sbt "run-main Example1"
sbt "run-main Example2"
```


